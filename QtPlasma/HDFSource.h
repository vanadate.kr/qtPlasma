#pragma once
#include "VolumeDataSource.h"

#include "hdf5.h"

class HDFSource :
	public VolumeDataSource
{
private:
	std::string m_fileName;

public:
	HDFSource(std::string file);
	virtual ~HDFSource();

	virtual void Load(std::string file);
};

