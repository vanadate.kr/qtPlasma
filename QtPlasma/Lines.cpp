#include "stdafx.h"
#include "Lines.h"

Lines::Lines()
{
	m_position = new std::vector<vec2>();
	m_index = new std::vector<int>();
	m_color = new std::vector<vec3>();
}

void Lines::Draw(float dt)
{
	if (m_position->size() == 0)
		return;

	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glScalef(0.5, 0.5, 1);

	glEnableClientState(GL_VERTEX_ARRAY);
	
	if (m_color->size() != 0)
		glEnableClientState(GL_COLOR_ARRAY);

	glVertexPointer(2, GL_FLOAT, 0, (*m_position)[0].Pointer());
	if (m_color->size() != 0)
		glColorPointer(3, GL_FLOAT, 0, (*m_color)[0].Pointer());

	if (m_index->size() != 0)
		glDrawElements(GL_LINES, m_index->size(), GL_INT, &(*m_index)[0]);
	else
		glDrawArrays(GL_LINES, 0, m_position->size());

	glDisableClientState(GL_VERTEX_ARRAY);

	glPopMatrix();
	glDisable(GL_DEPTH_TEST);
}

std::vector<vec2>& Lines::GetPosition()
{
	return *m_position;
}

std::vector<int>& Lines::GetIndex()
{
	if (!m_index)
		m_index = new std::vector < int > ;

	return *m_index;
}

std::vector<vec3>& Lines::GetColor()
{
	return *m_color;
}

Lines::~Lines()
{
	if (m_position)
		delete m_position;
	if (m_index)
		delete m_index;
	if (m_color)
		delete m_color;
}
