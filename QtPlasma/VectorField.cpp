#include "stdafx.h"

#include "VectorField.h"

VectorField::VectorField(std::vector<Vector3f>* data, const Vector3i& dims) : m_vector(data), m_dims(dims)
{
	m_fMaxLength = -1.0E25;
	m_fMinLength = 1.0E25;
	for (int i = 0; i < dims.x * dims.y * dims.z; i++) {
		float length = vec3((*data)[i].x, (*data)[i].y, (*data)[i].z).Length();
		if (length > m_fMaxLength)
			m_fMaxLength = length;
		if (length < m_fMinLength)
			m_fMinLength = length;
	}
}

VectorField::~VectorField()
{
	delete m_vector;
}

void VectorField::Draw(float dt)
{
	glMatrixMode(GL_MODELVIEW);
	glTranslatef(-m_dims.x / 2, 0, -m_dims.z / 2);
	for (int i = 0; i < m_dims.x; i++) {
		for (int j = 0; j < m_dims.y; j++) {
			for (int k = 0; k < m_dims.z; k++) {
				int index = i * m_dims.y * m_dims.z + j * m_dims.z + k;

				vec3 vec( (*m_vector)[index].y, (*m_vector)[index].x, (*m_vector)[index].z );
				float length = vec.Length();
				float f = length / (m_fMaxLength + m_fMinLength);
				vec3 color = vec3(1.0, 0, 0).Lerp(length, vec3(0, 0, 1.0));

     			glPushMatrix();
				glTranslatef(j, i, k);
				glBegin(GL_LINES);
				glColor3f(color.x, color.y, color.z);
				glVertex3f(0, 0, 0);
				glVertex3f(vec.x, vec.y, vec.z);
				glEnd();
				glPopMatrix();
			}
		}
	}
}