#include "stdafx.h"
#include "VolumeDataSource.h"

#include "mc_const.h"
#include "Surface.h"
#include "Lines.h"

#include <list>

unsigned int* VolumeDataSource::GetDimension()
{
	return m_dims;
}

float* VolumeDataSource::GetData()
{
	return m_data;
}

Surface* VolumeDataSource::ConstructSurface(float isoValue, float scale)
{
	Surface* surface = new Surface();

	for (int iX = 0; iX < m_dims[0] - 1; iX++)
		for (int iY = 0; iY < m_dims[1] - 1; iY++)
			for (int iZ = 0; iZ < m_dims[2] - 1; iZ++)
			{
				_MarchCube(surface, iX, iY, iZ, isoValue, scale);
			}

	return surface;
}

void VolumeDataSource::_MarchCube(Surface* surface, unsigned int x, unsigned int y, unsigned int z, float isoValue, float fScale)
{
	int iCorner, iVertex, iVertexTest, iEdge, iTriangle, iFlagIndex, iEdgeFlags;
	float fOffset;
	Vector3f sColor;
	float afCubeValue[8];
	Vector3f asEdgeVertex[12];
	Vector3f asEdgeNorm[12];

	//Make a local copy of the values at the cube's corners
	for (iVertex = 0; iVertex < 8; iVertex++)
	{
		afCubeValue[iVertex] = Sample(x + MC_CONST::fVertexOffset[iVertex][0],
			y + MC_CONST::fVertexOffset[iVertex][1],
			z + MC_CONST::fVertexOffset[iVertex][2]);
	}

	//Find which vertices are inside of the surface and which are outside
	iFlagIndex = 0;
	for (iVertexTest = 0; iVertexTest < 8; iVertexTest++)
	{
		if (afCubeValue[iVertexTest] <= isoValue)
			iFlagIndex |= 1 << iVertexTest;
	}

	//Find which edges are intersected by the surface
	iEdgeFlags = MC_CONST::iCubeEdgeFlags[iFlagIndex];

	//If the cube is entirely inside or outside of the surface, then there will be no intersections
	if (iEdgeFlags == 0)
	{
		return;
	}

	//Find the point of intersection of the surface with each edge
	//Then find the normal to the surface at those points
	for (iEdge = 0; iEdge < 12; iEdge++)
	{
		//if there is an intersection on this edge
		if (iEdgeFlags & (1 << iEdge))
		{
			double fDelta = afCubeValue[MC_CONST::iEdgeConnection[iEdge][1]] - afCubeValue[MC_CONST::iEdgeConnection[iEdge][0]];

			if (fDelta == 0.0)
			{
				fOffset = 0.5;
			}
			else
			{
				fOffset = (isoValue - afCubeValue[MC_CONST::iEdgeConnection[iEdge][0]]) / fDelta;
			}

			asEdgeVertex[iEdge].y = (x + MC_CONST::fVertexOffset[MC_CONST::iEdgeConnection[iEdge][0]][0] + fOffset * MC_CONST::fEdgeDirection[iEdge][0]) * fScale;
			asEdgeVertex[iEdge].x = (y + MC_CONST::fVertexOffset[MC_CONST::iEdgeConnection[iEdge][0]][1] + fOffset * MC_CONST::fEdgeDirection[iEdge][1]) * fScale;
			asEdgeVertex[iEdge].z = (z + MC_CONST::fVertexOffset[MC_CONST::iEdgeConnection[iEdge][0]][2] + fOffset * MC_CONST::fEdgeDirection[iEdge][2]) * fScale;
		}
	}


	//Draw the triangles that were found.  There can be up to five per cube
	for (iTriangle = 0; iTriangle < 5; iTriangle++)
	{
		if (MC_CONST::iTriangleConnectionTable[iFlagIndex][3 * iTriangle] < 0)
			break;

		Vector3f* triangle[3];
		for (iCorner = 0; iCorner < 3; iCorner++)
		{
			iVertex = MC_CONST::iTriangleConnectionTable[iFlagIndex][3 * iTriangle + iCorner];

			//vGetColor(sColor, asEdgeVertex[iVertex], asEdgeNorm[iVertex]);
			//glColor3f(sColor.fX, sColor.fY, sColor.fZ);
			surface->GetPosition().push_back(asEdgeVertex[iVertex]);
			triangle[iCorner] = &asEdgeVertex[iVertex];
		}

		vec3 normal = vec3(triangle[0]->x, triangle[0]->y, triangle[0]->z).Cross(vec3(triangle[1]->x, triangle[1]->y, triangle[1]->z)).Normalized();
		Vector3f conv;
		conv.x = normal.x;
		conv.y = normal.y;
		conv.z = normal.z;
		surface->GetNormal().push_back(conv);
		surface->GetNormal().push_back(conv);
		surface->GetNormal().push_back(conv);
	}
}

float VolumeDataSource::Sample(unsigned int x, unsigned int y, unsigned int z)
{
	if (x >= m_dims[0])
		x = m_dims[0] - 1;
	if (y >= m_dims[1])
		y = m_dims[1] - 1;
	if (z >= m_dims[2])
		z = m_dims[2] - 1;

	if (x < 0)
		x = 0;
	if (y < 0)
		y = 0;
	if (z < 0)
		z = 0;

	return m_data[x * m_dims[1] * m_dims[2] + y * m_dims[2] + z];
}

Surface* VolumeDataSource::CalculateAlphaShape(float alpha)
{
	Surface* surface = new Surface();
	if (as == nullptr) {
		Delaunay dt;
		int iX, iY, iZ;
		int count = 0;
		for (iX = 0; iX < m_dims[0] - 1; iX += 6)
			for (iY = 0; iY < m_dims[1] - 1; iY += 8)
				for (iZ = 0; iZ < m_dims[2] - 1; iZ += 8)
				{
					float value = m_data[iX * m_dims[1] * m_dims[2] + iY * m_dims[2] + iZ];
					if (value == 0.0f)
						continue;

					dt.insert(Point(iX, iY, iZ));
					count++;
				}

		as = new Alpha_shape_3(dt, Alpha_shape_3::GENERAL);
	}

	// compute alpha shape
	as->set_alpha(alpha * alpha);

	/// collect all regular facets (fetch regular facets from as and inserts in facets)
	std::vector<Alpha_shape_3::Facet> facets;
	as->get_alpha_shape_facets(std::back_inserter(facets), Alpha_shape_3::REGULAR);

	std::size_t nbf = facets.size();
	for (std::size_t i = 0; i < nbf; ++i)
	{
		//To have a consistent orientation of the facet, always consider an exterior cell
		if (as->classify(facets[i].first) != Alpha_shape_3::EXTERIOR)
			facets[i] = as->mirror_facet(facets[i]);

		int indices[3] = {
			(facets[i].second + 1) % 4,
			(facets[i].second + 2) % 4,
			(facets[i].second + 3) % 4,
		};

		/// according to the encoding of vertex indices, this is needed to get
		/// a consistent orienation
		if (facets[i].second % 2 == 0) std::swap(indices[0], indices[1]);

		Vector3f vertex1, vertex2, vertex3;

		vertex1.x = facets[i].first->vertex(indices[0])->point().x();
		vertex1.y = facets[i].first->vertex(indices[0])->point().y();
		vertex1.z = facets[i].first->vertex(indices[0])->point().z();
		surface->GetPosition().push_back(vertex1);

		vertex2.x = facets[i].first->vertex(indices[1])->point().x();
		vertex2.y = facets[i].first->vertex(indices[1])->point().y();
		vertex2.z = facets[i].first->vertex(indices[1])->point().z();
		surface->GetPosition().push_back(vertex2);

		vertex3.x = facets[i].first->vertex(indices[2])->point().x();
		vertex3.y = facets[i].first->vertex(indices[2])->point().y();
		vertex3.z = facets[i].first->vertex(indices[2])->point().z();
		surface->GetPosition().push_back(vertex3);

		vec3 vec1 = vec3(vertex1.x, vertex1.y, vertex3.z) - vec3(vertex2.x, vertex2.y, vertex2.z);
		vec3 vec2 = vec3(vertex1.x, vertex1.y, vertex3.z) - vec3(vertex3.x, vertex3.y, vertex3.z);
		vec3 normal = vec1.Cross(vec2).Normalized();
		Vector3f conv;
		conv.x = normal.x;
		conv.y = normal.y;
		conv.z = normal.z;
		surface->GetNormal().push_back(conv);
		surface->GetNormal().push_back(conv);
		surface->GetNormal().push_back(conv);

		surface->GetIndex().push_back(3 * i + 1);
		surface->GetIndex().push_back(3 * i + 2);
		surface->GetIndex().push_back(3 * i + 3);
	}

	return surface;
}

Lines* VolumeDataSource::CalculateAlphaShape2(float minValue, float maxValue, float alpha, int slice)
{
	Lines* line = new Lines();
	std::list<Point2> points;

	int iX, iZ;
	float min = 1.0e30;
	float max = 0;
	int count = 0;
	for (iX = 0; iX < m_dims[0]; iX ++)
		for (iZ = 0; iZ < m_dims[2]; iZ ++) {
			float value = m_data[iX * m_dims[1] * m_dims[2] + slice * m_dims[2] + iZ];
			if (value < min)
				min = value;
			if (value > max)
				max = value;

			if (value == 0.0f || !(value >= minValue && value <= maxValue))
				continue;

			points.push_back(Point2(iX, iZ));
			count++;
		}

	Alpha_shape_2 A(points.begin(), points.end(),
		FT(alpha),
		Alpha_shape_2::GENERAL);

	for (Alpha_shape_edges_iterator it = A.alpha_shape_edges_begin(); it != A.alpha_shape_edges_end(); ++it) {
		auto p1 = A.segment(*it).vertex(0);
		auto p2 = A.segment(*it).vertex(1);
		line->GetPosition().push_back(vec2(p1.x(), p1.y()));
		line->GetPosition().push_back(vec2(p2.x(), p2.y()));
	}

	return line;
}

Volume<float>* VolumeDataSource::GetVolume(ivec3 color)
{
	Volume<float>* volume = new Volume<float>(m_data, m_dims, color);

	return volume;
}

Volume<float>* VolumeDataSource::GetVolume() {
	return VolumeDataSource::GetVolume(ivec3(255,255,255));
}

VolumeDataSource::~VolumeDataSource()
{
	if (as != nullptr)
		delete as;
}

VolumeDataSource::VolumeDataSource()
{
	as = nullptr;
}
