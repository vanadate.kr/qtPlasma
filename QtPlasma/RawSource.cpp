#include "stdafx.h"
#include <fstream>

#include "RawSource.h"

RawSource::RawSource(std::string file, int dimX, int dimY, int dimZ)
{
	m_type = RAW;
	m_fileName = file.substr(file.find_last_of("/") + 1);
	m_data = nullptr;

	m_dims[0] = dimX;
	m_dims[1] = dimY;
	m_dims[2] = dimZ;

	Load(file);
}

RawSource::~RawSource()
{
	if (m_data != nullptr)
		delete[] m_data;
}

void RawSource::Load(std::string file)
{
	std::ifstream inFile(file, std::ios_base::binary);

	inFile.seekg(0, std::ios::end);
	unsigned long long size = inFile.tellg();
	inFile.seekg(0, std::ios::beg);

	m_data = new float[size];
	inFile.read((char*)(m_data), size);
	inFile.close();
}
