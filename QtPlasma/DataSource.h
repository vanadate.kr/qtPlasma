#pragma once
#include <string>

#include "types.h"

class DataSource
{
protected:
	DataType m_type;

public:
	virtual void Load(std::string file) = 0;
};