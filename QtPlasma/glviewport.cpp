#include "stdafx.h"
#include "glviewport.h"

#include "Scene.h"

GLViewport::GLViewport(QWidget *parent)
	: QGLWidget(parent)
{
	setFormat(QGLFormat(QGL::DoubleBuffer | QGL::DepthBuffer));
}


GLViewport::~GLViewport()
{
}

void GLViewport::initializeGL()
{
	qglClearColor(Qt::black);
	m_cam.SetView(vec3(0, 0, 30), vec3(0, 0, 0), vec3(0, 1, 0));

	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);

	static GLfloat lightPosition[4] = { 0, 0, 10, 1.0 };
	static GLfloat AmbientColor[] = { 1.0f, 0.8f, 0.0f, 0.0f };         //주변광
	static GLfloat DiffuseColor[] = { 0.5f, 0.5f, 0.0f, 0.0f };          //분산광

	// LIGHT0 설정
	glLightfv(GL_LIGHT0, GL_AMBIENT, AmbientColor);       // 주변광 성분 설정
	glLightfv(GL_LIGHT0, GL_DIFFUSE, DiffuseColor);       // 분산광 성분 설정
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);     // 광원 위치 설정
}

void GLViewport::resizeGL(int width, int height)
{
	glViewport(0, 0, width, height);

	GLfloat fAspect = (GLfloat)width / (GLfloat)height;
	//m_cam.SetProj(mat4::Ortho(-100.0, 100.0, -100.0, 100.0, -100.0, 100.0));
	m_cam.SetProj(mat4::Perspective(60.0f, fAspect, 1.0f, 10000.0f));
	m_cam.SetMatrix();
}

void GLViewport::DrawAxis()
{
	m_cam.SetMatrix();

	// 축을 그린다
	glBegin(GL_LINES);
	glColor3f(1, 0, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(100, 0, 0);
	glColor3f(0, 1, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 100, 0);
	glColor3f(0, 0, 1);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 0, 100);
	glEnd();

	qglColor(Qt::white);
	renderText(10, 20, QString("Untitled"));
}

void GLViewport::paintGL()
{
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// 장면을 그린다
	Scene::GetInstance().Draw(0.0);
	DrawAxis();
}

void GLViewport::mousePressEvent(QMouseEvent *event)
{
	m_prvPos = event->pos();
}

void GLViewport::mouseMoveEvent(QMouseEvent *event)
{
	int dx = event->x() - m_prvPos.x();
	int dy = event->y() - m_prvPos.y();
	
	m_cam.Pan(-dy, -dx);
	m_prvPos = event->pos();

	updateGL();
}

void GLViewport::wheelEvent(QWheelEvent *event)
{
	m_cam.Zoom(-event->delta());
	updateGL();
}
