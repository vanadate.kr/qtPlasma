#include "stdafx.h"
#include "HDFSource.h"


HDFSource::HDFSource(std::string file)
{
	m_type = HDF;
	m_fileName = file.substr(file.find_last_of("/") + 1);
	m_data = nullptr;

	Load(file);
}


HDFSource::~HDFSource()
{
	if (m_data != nullptr)
		delete[] m_data;
}

void HDFSource::Load(std::string file)
{
	hid_t hdf5_fp = H5Fopen(file.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
	hid_t hdf5_dataset = H5Dopen(hdf5_fp, "NDF", H5P_DEFAULT);
	hid_t hdf5_datatype = H5Dget_type(hdf5_dataset);
	size_t data_size = H5Tget_size(hdf5_datatype);
	hid_t hdf5_dataspace = H5Dget_space(hdf5_dataset);

	hsize_t dims[3];
	H5Sget_simple_extent_dims(hdf5_dataspace, dims, NULL);
	m_dims[0] = dims[0];
	m_dims[1] = dims[1];
	m_dims[2] = dims[2];

	m_data = new float[m_dims[2] * m_dims[1] * m_dims[0]]; // row*column*height

	herr_t hdf5_status = H5Dread(hdf5_dataset, H5T_IEEE_F32LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, m_data);
	H5Dclose(hdf5_dataset);
	H5Sclose(hdf5_dataspace);
	H5Fclose(hdf5_fp);
}
