#include "stdafx.h"
#include "Volume.h"


Volume::Volume(float* data, const ivec3& dims)
{
// 	m_position.reserve(dims.x * dims.y * dims.z);
// 	for (int i = 0; i < dims.x; i++) {
// 		for (int j = 0; j < dims.y; j++) {
// 			for (int k = 0; k < dims.z; k++) {
// 				m_position.push_back(vec3(-10.0f + k * 0.05f, -2.0f + i * 0.05f, -10.0f + j * 0.05f));
// 			}
// 		}
// 	}

	int x = 200;

	float max = 0.0f;
	for (int i = 0; i < dims.y; i++) {
		for (int j = 0; j < dims.z; j++) {
			float height = data[x * dims.y * dims.z + i * dims.z + j];
			if (height > max)
				max = height;
		}
	}

	m_position.reserve(dims.y * dims.z);
	for (int i = 0; i < dims.y; i++) {
		for (int j = 0; j < dims.z; j++) {
			float height = data[x * dims.y * dims.z + i * dims.z + j] / max * 12;
			m_position.push_back(vec3(-10.0f + i * 0.05f, height, -10.0f + j * 0.05f));
		}
	}
}


Volume::~Volume()
{

}

void Volume::Draw(float dt)
{
	glEnableClientState(GL_VERTEX_ARRAY);

	glColor3f(0.0, 0.0, 1.0);
	glVertexPointer(3, GL_FLOAT, 0, m_position[0].Pointer());
	glDrawArrays(GL_POINTS, 0, m_position.size());

	glDisableClientState(GL_VERTEX_ARRAY);
}
