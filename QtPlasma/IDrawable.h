#pragma once
class IDrawable
{
protected:
	IDrawable() {};

public:
	virtual ~IDrawable() {};
	virtual void Update(float dt) = 0;
	virtual void Draw(float dt) = 0;
};

