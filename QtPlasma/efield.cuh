#pragma once
#include <vector>
#include "types.h"

#include "cuda_common.h"

extern "C" std::vector<Vector3f>* CalculateEField(unsigned int dimX, unsigned int dimY, unsigned int dimZ, float* ion, float* electron, Vector3i outputDim);