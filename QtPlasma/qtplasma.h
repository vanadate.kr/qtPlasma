#ifndef QTPLASMA_H
#define QTPLASMA_H

#include <QtWidgets/QMainWindow>
#include "ui_qtplasma.h"

class Importer;
class Scene;

class QtPlasma : public QMainWindow
{
	Q_OBJECT

public:
	QtPlasma(QWidget *parent = 0);
	~QtPlasma();

private:
	Ui::QtPlasmaClass ui;
	Importer& m_importer;
	Scene& m_scene;
	QOpenGLContext* m_pContext;

	int alpha;

	float* m_useData;

	void _AddDataSource();

public slots:
	void OnOpenMenuClicked();
	void OnWireframeMenuClicked(bool checked);
	void OnVolumeBtnClicked();
	void OnEFieldBtnClicked();
	void OnIsosufaceBtnClicked();
	void OnApplyBtnClicked();
	void OnAlphaShapeBtnClicked();
	void OnAlphaChanged(int value);
};

#endif // QTPLASMA_H
