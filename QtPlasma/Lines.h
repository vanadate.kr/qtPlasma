#pragma once
#include "IDrawable.h"
#include <vector>
#include "Vector.hpp"

class Lines :
	public IDrawable
{
private:
	std::vector<vec2>* m_position;
	std::vector<int>* m_index;
	std::vector<vec3>* m_color;

public:
	Lines();
	virtual ~Lines();

	std::vector<vec2>& GetPosition();
	std::vector<int>& GetIndex();
	std::vector<vec3>& GetColor();

	// Implement virtual methods
	virtual void Update(float dt) {};
	virtual void Draw(float dt);
};