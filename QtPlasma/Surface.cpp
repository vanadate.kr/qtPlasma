#include "stdafx.h"
#include "Surface.h"                                                         Point;

Surface::Surface(std::vector<Vector3f>* position)
{
	m_normal = nullptr;
}

Surface::Surface()
{
	m_position = new std::vector<Vector3f>();
	m_index = new std::vector<int>();
	m_normal = new std::vector<Vector3f>();
	m_color = new std::vector<Vector3f>();
}

Surface::~Surface()
{
	if (m_position)
		delete m_position;
	if (m_index)
		delete m_index;
	if (m_normal)
		delete m_normal;
	if (m_color)
		delete m_color;
}

void Surface::Draw(float dt)
{
	if (m_normal->size() != 0) {
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
	}
	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(-12, 0, -12);
	
	glEnableClientState(GL_VERTEX_ARRAY);
	if (m_normal->size() != 0)
		glEnableClientState(GL_NORMAL_ARRAY);

	if (m_color->size() != 0)
		glEnableClientState(GL_COLOR_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, &(*m_position)[0]);
	if (m_normal->size() != 0)
		glNormalPointer(GL_FLOAT, 0, &(*m_normal)[0]);
	if (m_color->size() != 0)
		glColorPointer(3, GL_FLOAT, 0, &(*m_color)[0]);
	
// 	if (m_index->size() != 0)
// 		glDrawElements(GL_TRIANGLES, m_index->size(), GL_INT, &(*m_index)[0]);
// 	else
	if (m_position->size() > 0)
		glDrawArrays(GL_TRIANGLES, 0, m_position->size());
		

	glDisableClientState(GL_VERTEX_ARRAY);
	if (m_normal->size() != 0)
		glDisableClientState(GL_NORMAL_ARRAY);

	
	glPopMatrix();
	
	if (m_normal->size() != 0) {
		glDisable(GL_LIGHTING);
		glDisable(GL_LIGHT0);
	}
	glDisable(GL_DEPTH_TEST);
}

std::vector<Vector3f>& Surface::GetPosition()
{
	return *m_position;
}

std::vector<Vector3f>& Surface::GetNormal()
{
	if (!m_normal)
		m_normal = new std::vector < Vector3f > ;

	return *m_normal;
}

std::vector<int>& Surface::GetIndex()
{
	if (!m_index)
		m_index = new std::vector < int >;

	return *m_index;
}

std::vector<Vector3f>& Surface::GetColor()
{
	if (!m_color)
		m_color = new std::vector < Vector3f > ;

	return *m_color;
}
