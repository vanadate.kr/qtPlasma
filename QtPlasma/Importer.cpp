#include "stdafx.h"
#include "Importer.h"

#include "HDFSource.h"
#include "RawSource.h"

Importer::~Importer()
{
	for (auto i = m_sources.begin(); i != m_sources.end(); i++) {
		delete *i;
	}
}

Importer& Importer::GetInstance()
{
	return instance;
}

void Importer::Load(std::string file, DataType type)
{
	bool success = true;
	switch (type)
	{
	case HDF:
		m_sources.push_back(new HDFSource(file));
		break;
	case RAW:
		m_sources.push_back(new RawSource(file, 335, 1000, 335));
		break;
	default:
		success = false;
		break;
	}

	if (success) {
		QTreeWidgetItem *treeRoot = new QTreeWidgetItem(m_treeDataSoure);
		treeRoot->setText(0, QString::fromStdString(file.substr(file.find_last_of("/") + 1)));
// 		QTreeWidgetItem *treeItem = new QTreeWidgetItem();
// 		treeItem->setText(0, QString("NDF"));
// 		treeItem->setText(1, QString("Volume"));
// 		treeRoot->addChild(treeItem);
	}
}

void Importer::SetDataSourceTree(QTreeWidget* tree)
{
	m_treeDataSoure = tree;
}

std::vector<DataSource*>& Importer::GetDataSources()
{
	return m_sources;
}

void Importer::Empty()
{
	for (auto i = m_sources.begin(); i != m_sources.end(); i++) {
		delete *i;
	}

	m_sources.clear();
	m_treeDataSoure->clear();
}

Importer Importer::instance;