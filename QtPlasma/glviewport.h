#ifndef GLVIEWPORT_H
#define GLVIEWPORT_H

#include <QGLWidget>

#include "Camera.h"

class GLViewport : public QGLWidget
{
	Q_OBJECT

public:
	GLViewport(QWidget *parent);
	~GLViewport();

protected:
	void initializeGL();
	void resizeGL(int width, int height);
	void paintGL();
	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void wheelEvent(QWheelEvent *event);

	void DrawAxis();
private:
	Camera m_cam;
	QPoint m_prvPos;
};

#endif // GLVIEWPORT_H