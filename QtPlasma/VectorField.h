#pragma once
#include "IDrawable.h"
#include <vector>
#include "Vector.hpp"
#include "types.h"

class VectorField :
	public IDrawable
{
private:
	std::vector<Vector3f>* m_vector;
	Vector3i m_dims;
	float m_fMaxLength;
	float m_fMinLength;

public:
	VectorField(std::vector<Vector3f>* data, const Vector3i& dims);
	virtual ~VectorField();

	// Implement virtual methods
	virtual void Update(float dt) {};
	virtual void Draw(float dt);
};

