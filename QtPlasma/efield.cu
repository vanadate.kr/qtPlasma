#include "efield.cuh"

#include <cuda_runtime.h>
#include <cufft.h>

#include "Timer.hpp"

__constant__ unsigned int cDataLength;
__constant__ unsigned int cFFTDataLength;
__constant__ unsigned int cDims[3];

__global__ void ChargeDensity(float* ion, float* electron, float* output) {
	unsigned int idx = blockIdx.x*blockDim.x + threadIdx.x;
	if (idx < cDataLength)
		output[idx] = (ion[idx] - electron[idx])/1.0e26;
}

__global__ void ConvolutionKernel(float* output) {
	if (blockIdx.x < cDims[0] && blockIdx.y < cDims[1] && threadIdx.x < cDims[2]) {
		unsigned int idx = blockIdx.x * cDims[1] * cDims[2] + blockIdx.y * cDims[2] + threadIdx.x;
		
		float i = 5.0e-2 * blockIdx.x;
		float j = 5.0e-2 * blockIdx.y;
		float k = 5.0e-2 * threadIdx.x;

		float sq = i*i;
		sq += j*j;
		sq += k*k;
		output[idx] = 1 / sqrtf(sq);
	}
}

__global__ void PointwiseMult(cufftComplex* output, cufftComplex* data, cufftComplex* kernel) {
	unsigned int idx = blockIdx.x*blockDim.x + threadIdx.x;
	if (idx < cFFTDataLength) {
		output[idx].x = data[idx].x * kernel[idx].x - data[idx].y * kernel[idx].y;
		output[idx].y = data[idx].x * kernel[idx].y + data[idx].y * kernel[idx].x;
	}
}


extern "C" std::vector<Vector3f>* CalculateEField(unsigned int dimX, unsigned int dimY, unsigned int dimZ, float* ion, float* electron, Vector3i outputDim) {
	float* device_ion;
	float* device_electron;
	float* device_chargeDensity;
	float* device_convolutionKernel;
	float* device_electricPotential;

	cufftComplex* device_DataSpectrum;
	cufftComplex* device_KernelSpectrum;
	cufftComplex* device_MultSpectrum;

	float* host_output;

	// 메모리 할당
	unsigned int dataLength = dimX * dimY * dimZ;
	unsigned int fftOutputLength = dimX * dimY * (dimZ / 2 + 1);

	unsigned int dims[3] = {dimX, dimY, dimZ};
	cudaMemcpyToSymbol(cDataLength, &dataLength, sizeof(unsigned int));
	cudaMemcpyToSymbol(cFFTDataLength, &fftOutputLength, sizeof(unsigned int));
	cudaMemcpyToSymbol(cDims, dims, sizeof(dims));
	
	host_output = new float[dataLength];

	// Device 메모리 할당
	// // Charge density
	cudaMalloc((void**)&device_ion, dataLength * sizeof(float));
	cudaMalloc((void**)&device_electron, dataLength * sizeof(float));
	cudaMalloc((void**)&device_chargeDensity, dataLength * sizeof(float));
	// // Convolution kernel
	cudaMalloc((void**)&device_convolutionKernel, dataLength * sizeof(float));
	// // Electric potential
	cudaMalloc((void**)&device_electricPotential, dataLength * sizeof(float));
	// // FFT output
	cudaMalloc((void **)&device_DataSpectrum, fftOutputLength * sizeof(cufftComplex));
	cudaMalloc((void **)&device_KernelSpectrum, fftOutputLength * sizeof(cufftComplex));
	cudaMalloc((void **)&device_MultSpectrum, fftOutputLength * sizeof(cufftComplex));

	// 타이머 생성
	StopWatchInterface *hTimer = NULL;
	sdkCreateTimer(&hTimer);

	// 타이머 시작
	sdkResetTimer(&hTimer);
	sdkStartTimer(&hTimer);

	// 파티클 정보 복사
	cudaMemcpy(device_ion, ion, dataLength * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(device_electron, electron, dataLength * sizeof(float), cudaMemcpyHostToDevice);

	// 타이머 정지
	sdkStopTimer(&hTimer);
	double copyTime = sdkGetTimerValue(&hTimer);

	// Charge density 계산
	dim3 cd_threads(1024, 1, 1);
	dim3 cd_grid(iDivUp(dataLength, 1024), 1, 1);
	ChargeDensity<<<cd_grid, cd_threads>>>(device_ion, device_electron, device_chargeDensity);

	// Convolution kernel 계산
	dim3 ck_threads(iDivUp(dimZ, 64) * 64, 1, 1);
	dim3 ck_grid(dimX, dimY, 1);
	ConvolutionKernel<<<ck_grid, ck_threads>>>(device_convolutionKernel);
	cudaMemcpy(device_convolutionKernel, device_convolutionKernel + 1, sizeof(float), cudaMemcpyDeviceToDevice);

	// 타이머 시작
	sdkResetTimer(&hTimer);
	sdkStartTimer(&hTimer);

	// FFT
	// // Plan 생성
	cufftHandle fftPlanFwd, fftPlanInv;
	cufftPlan3d(&fftPlanFwd, dimX, dimY, dimZ, CUFFT_R2C);
	cufftPlan3d(&fftPlanInv, dimX, dimY, dimZ, CUFFT_C2R);
	// // 정방향 FFT
	cufftExecR2C(fftPlanFwd, (cufftReal *)device_chargeDensity, device_DataSpectrum);
	cufftExecR2C(fftPlanFwd, (cufftReal *)device_convolutionKernel, device_KernelSpectrum);
	// // Pointwise mult.
	cudaDeviceSynchronize();
	PointwiseMult<<<iDivUp(fftOutputLength, 512), 512>>>(device_MultSpectrum, device_DataSpectrum, device_KernelSpectrum);
	// // 역방향 FFT
	cudaDeviceSynchronize();
	cufftExecC2R(fftPlanInv, device_MultSpectrum, (cufftReal *)device_electricPotential);
	cudaDeviceSynchronize();

	// 타이머 정지
	sdkStopTimer(&hTimer);
	double gpuTime = sdkGetTimerValue(&hTimer);
	printf("Convolution with CUDA FFT : %f ms\n", gpuTime);

	// 결과 복사
	// 타이머 시작
	sdkResetTimer(&hTimer);
	sdkStartTimer(&hTimer);
	//cudaMemcpy(host_output, device_electricPotential, dataLength * sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(host_output, device_chargeDensity, dataLength * sizeof(float), cudaMemcpyDeviceToHost);
	// 타이머 정지
	sdkStopTimer(&hTimer);
	copyTime += sdkGetTimerValue(&hTimer);

	// 전기장 계산
	int xOffset = dimX / outputDim.x;
	int yOffset = dimY / outputDim.y;
	int zOffset = dimZ / outputDim.z;
	auto result = new std::vector<Vector3f>();
	for (int i = 0; i < dimX; i += xOffset) {
		for (int j = 0; j < dimY; j += yOffset) {
			for (int k = 0; k < dimZ; k += zOffset) {
				Vector3f E;
				int curIndex = i * dimY * dimZ + j * dimZ + k;
				E.x = (host_output[curIndex + dimY * dimZ] - host_output[curIndex]) / 1.0E12;
				E.y = (host_output[curIndex + dimZ] - host_output[curIndex]) / 1.0E12;
				E.z = (host_output[curIndex + 1] - host_output[curIndex]) / 1.0E12;

				result->push_back(E);
			}
		}
	}

	// 메모리 해제
	cudaFree(device_ion);
	cudaFree(device_electron);
	cudaFree(device_chargeDensity);
	cudaFree(device_DataSpectrum);
	cudaFree(device_KernelSpectrum);
	cudaFree(device_MultSpectrum);
	cudaFree(device_convolutionKernel);
	cudaFree(device_electricPotential);

	delete[] host_output;

	// Plat 삭제
	cufftDestroy(fftPlanFwd);
	cufftDestroy(fftPlanInv);

	sdkDeleteTimer(&hTimer);
	
	return result;
}