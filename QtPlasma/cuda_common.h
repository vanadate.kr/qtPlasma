#pragma once
typedef struct
{
	float x;
	float y;
} fComplex;

inline int iDivUp(int a, int b)
{
	return (a % b != 0) ? (a / b + 1) : (a / b);
}