#pragma once
#include "IDrawable.h"
#include "Vector.hpp"

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#define MAP_3DTEXT( TexIndex, w, h, d ) \
glTexCoord3f(0.0f, 0.0f, TexIndex);  \
glVertex3f(0,0,d*TexIndex);\
glTexCoord3f(1.0f, 0.0f, TexIndex);  \
glVertex3f(w,0,d*TexIndex);\
glTexCoord3f(1.0f, 1.0f, TexIndex);  \
glVertex3f(w,h,d*TexIndex);\
glTexCoord3f(0.0f, 1.0f, TexIndex);  \
glVertex3f(0,h,d*TexIndex);

template <class T>
class Volume :
	public IDrawable
{
private:
	unsigned int m_dims[3];
	GLuint volumeTexture;

public:
	Volume(const T* data, const unsigned int* dims, ivec3 color) {
		memcpy(m_dims, dims, sizeof(unsigned) * 3);
		// 3D Texture & Alpha blending
		PFNGLTEXIMAGE3DPROC glTexImage3D = (PFNGLTEXIMAGE3DPROC)wglGetProcAddress("glTexImage3D");
		glGenTextures(1, &volumeTexture);

		float max = 0;
		float mini = 0;
		for (int i = 0; i < dims[0] * dims[1] * dims[2]; i++) {
			if (data[i] > max)
				max = data[i];
			if (data[i] < mini)
				mini = data[i];
		}

		float offset = mini * -1;

		Vector4<BYTE>* rgbaBuffer = new Vector4<BYTE>[dims[0] * dims[1] * dims[2]];
		for (int i = 0; i < dims[0]; i++) {
			for (int j = 0; j < dims[1]; j++) {
				for (int k = 0; k < dims[2]; k++) {
					BYTE alpha = min(((data[i * dims[2] * dims[1] + j * dims[2] + k] + offset) / (max + offset) * 255), 255);

					//Vector3<BYTE> lerp = Vector3<BYTE>(0, 0, 255).Lerp(color / 255.0f, Vector3<BYTE>(255, 0, 0));
					rgbaBuffer[i * dims[2] * dims[1] + j * dims[2] + k] = Vector4<BYTE>(color.x, color.y, color.z, alpha);

				}
			}
		}

		glBindTexture(GL_TEXTURE_3D, volumeTexture);
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, dims[1], dims[2], dims[0], 0, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid *)&rgbaBuffer[0]);

		glBindTexture(GL_TEXTURE_3D, 0);
	};
	virtual ~Volume() {};

	// Implement virtual methods
	virtual void Update(float dt) {};
	virtual void Draw(float dt) {
		

		glEnable(GL_TEXTURE_3D);
		glEnable(GL_ALPHA_TEST);
		glAlphaFunc(GL_GREATER, 0.03f);

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDisable(GL_CULL_FACE);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glBindTexture(GL_TEXTURE_3D, volumeTexture);
		GLenum err = glGetError();


		glMatrixMode(GL_TEXTURE);
		glLoadIdentity();
		// Translate and make 0.5f as the center 
		// (texture co ordinate is from 0 to 1.
		// so center of rotation has to be 0.5f)
		glTranslatef(0.5f, 0.5f, 0.5f);
		glRotated(1.5, 0, 1.0, 0);
		glTranslatef(-0.5f, -0.5f, -0.5f);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glTranslatef(-1 * (float)m_dims[1] / 2, -1 * (float)m_dims[2] / 2, -1 * (float)m_dims[0] / 2);
		for (int i = 0; i <= m_dims[2]; i++)
		{
			glBegin(GL_QUADS);
			MAP_3DTEXT((float)i / m_dims[0], m_dims[1], m_dims[2], m_dims[0]);
			glEnd();
		}
		glPopMatrix();
		glBindTexture(GL_TEXTURE_3D, 0);

		glDisable(GL_BLEND);
		glDisable(GL_ALPHA_TEST);
		glDisable(GL_TEXTURE_3D);
		glEnable(GL_CULL_FACE);
	};
};

